$(document).ready(function() {
    $('.slider').slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 10000
    });
    $('input').iCheck({
        labelHover: false,
        cursor: true
    });
    $('.lightbox').nivoLightbox();
});
$('#show-password').on('ifChanged', function(event){
//$('#show-password').change(function(){
  $('#password').hideShowPassword($(this).prop('checked'));
});